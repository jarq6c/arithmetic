from setuptools import setup, find_namespace_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name='arithmetic-addition',
    version='2.0.0',
    description='Add NumPy arrays.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/jarq6c/arithmetic',
    author='Jason A. Regina',
    author_email='jarq6c@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    keywords='mathematics, arithmetic',
    packages=find_namespace_packages(include=['arithmetic.*']),
    python_requires='>=3.6',
    install_requires=['numpy']
)
